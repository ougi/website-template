const gulp = require("gulp");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");
const ejs = require("gulp-ejs");
const browserSync = require("browser-sync").create();
const sassGlob = require("gulp-sass-glob");
const rimraf = require("rimraf");
const rename = require("gulp-rename");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");

gulp.task("ejs", cb => {
  return gulp
    .src([
      "./src/ejs/**/*.ejs",
      "!./src/ejs/_includes/**/*",
      "!./src/ejs/**/_*.ejs"
    ])
    .pipe(ejs())
    .pipe(rename({ extname: "" }))
    .pipe(gulp.dest("./dist"))
    .pipe(browserSync.stream());
});

gulp.task("sass", cb => {
  return gulp
    .src("./src/scss/main.scss")
    .pipe(sassGlob())
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write("."))
    .pipe(rename({ suffix: ".bundle" }))
    .pipe(gulp.dest("./dist/assets/css"))
    .pipe(browserSync.stream());
});

gulp.task("js:vendor", cb => {
  return gulp
    .src([
      "./src/js/vendor/jquery-3.4.1.min.js",
      "./src/js/vendor/jquery.magnific-popup.min.js",
      "./src/js/vendor/gsap/gsap.min.js"
    ])
    .pipe(concat("vendor.bundle.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/assets/js"));
});

gulp.task("js", cb => {
  return gulp
    .src(["./src/js/main.js"])
    .pipe(concat("main.bundle.js"))
    .pipe(uglify())
    .pipe(gulp.dest("./dist/assets/js"))
    .pipe(browserSync.stream());
});

gulp.task("media", cb => {
  return gulp.src("./src/media/**/*").pipe(gulp.dest("./dist/assets/media"));
});

gulp.task("clean", function(cb) {
  return rimraf("./dist", cb);
});

gulp.task("serve", cb => {
  browserSync.init({
    server: "./dist"
  });
  gulp.watch("./src/ejs/**/*", gulp.task("ejs"));
  gulp.watch("./src/scss/**/*", gulp.task("sass"));
  gulp.watch("./src/js/**/*", gulp.task("js"));
  gulp.watch("./src/media/**/*", gulp.task("media"));
});

gulp.task(
  "build",
  gulp.series("clean", "media", "ejs", "sass", "js:vendor", "js")
);
gulp.task("start", gulp.series("build", "serve"));
