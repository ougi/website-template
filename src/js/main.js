// Global Namespace
var NS = {};

// Create child namespaces
NS.ns = function(ns_str) {
  var parts = ns_str.split(".");
  var parent = NS;
  var i = undefined;
  if (parts[0] === "NS") {
    parts = parts.slice(1);
  }
  for (i = 0; i < parts.length; i += 1) {
    if (typeof parent[parts[i]] === "undefined") {
      parent[parts[i]] = {};
    }
    parent = parent[parts[i]];
  }
  return parent;
};

// Endpoint
$(function() {
  console.log("Hello World");
});
